// The handlers for the main file's functions.
package main

import (
	"fmt"
	"io/fs"
	"os"
)

// Checks if a journal exists.
func ensureJournalExists(path string) bool {
	fmt.Println("Searching for journal '" + path + ".tsj'...")
	_, err := os.Stat(path + ".tsj")
	if err != nil {
		return false
	}
	return true
}

// Checks if a path exists.
func ensurePathExists(rawPath string) bool {
	fmt.Println("Searching for required file '" + rawPath + "'...")
	_, err := os.Stat(rawPath)
	if err != nil {
		return false
	}
	return true
}

// Causes the program to exit with an error code if a required file does not exist.
func dieIfNotExists(rawPath string) {
	if !ensurePathExists(rawPath) {
		fmt.Println("Error locating required file '" + rawPath + "'!")
		os.Exit(-30)
	}
}

// Checks if a text file exists.
func ensureTextExists(path string) bool {
	fmt.Println("Searching for text file '" + path + ".txt'...")
	_, err := os.Stat(path + ".txt")
	if err != nil {
		return false
	}
	return true
}

// Creates a new journal at a specified location.
func HandleNew(result CommandLineResult) {
	fmt.Println("Creating new journal called", result.Name, "in ./")
	if ExistsFile(result.Name + ".tsj") {
		fmt.Println("Error: Journal already exists.")
		fmt.Println("Abort.")
		return
	} else {
		os.Create(result.Name + ".tsj")
		if !ExistsFile(result.Name + ".tsj") {
			fmt.Println("Error: Could not create file.")
			os.Exit(-15)
		}
		jrnl := NewJournalNoSeed(result.NewTitle, result.User)
		//jrnl.AddEntry(libtsj.NewJournalEntry("An Entry", "Once upon a time, some stuff happened..."))
		fmt.Println("Writing data...")
		if !jrnl.Write(result.Name) {
			fmt.Println("Error writing journal '" + result.Name + ".tsj'!")
			os.Exit(-16)
		}
		fmt.Println("Wrote journal to '" + result.Name + ".tsj'!")
	}
}

// Adds an entry to the specified journal.
func HandleAddEntry(result CommandLineResult) {
	dieIfNotExists(result.Name + ".tsj")
	fmt.Println("Found! Adding Entry...")
	if result.File == "NULL" {
		fmt.Println("Error: path must not be null and must be a readable text file.")
		os.Exit(-31)
	}
	if result.Subject == "NULL" {
		fmt.Println("Error: subject must not be null.")
		os.Exit(-32)
	}
	if !ensureTextExists(result.File) {
		fmt.Println("Error: " + result.File + ".txt does not exist. Please try again.")
		os.Exit(-33)
	} else {
		fmt.Println("Found! Importing entry...")
	}
	jrnl := ReadJournal(result.Name)
	txt, err := os.ReadFile(result.File + ".txt")
	if err != nil {
		fmt.Println("Error: Could not read " + result.File + ".txt")
		os.Exit(-34)
	}
	jrnl.AddEntry(NewJournalEntry(result.Subject, string(txt)))
	if !result.AEKeep {
		fmt.Println("Removing original entry text file...")
		os.Remove(result.File + ".txt")
		fmt.Println("Done.")
	} else {
		fmt.Println("Keeping original entry text file.")
	}
	if !jrnl.Write(result.Name) {
		fmt.Println("Wrote entry '" + result.Subject + "' to ./" + result.Name + ".tsj")
		os.Exit(0)
	}
}

//Handles the ability to list entries
func HandleListEntries(result CommandLineResult) {
	dieIfNotExists(result.Name + ".tsj")
	fmt.Println("Found! Loading journal...")
	jrnl := ReadJournal(result.Name)
	fmt.Println("Loaded!")
	for i := 0; i < len(jrnl.Entries); i++ {
		fmt.Println("Entry '" + jrnl.Entries[i].Subject + "' <" + jrnl.Entries[i].Hash + ">")
	}
}

// Handles the ability to display the contents of a given entry by subject.
func HandleViewEntry(result CommandLineResult) {
	dieIfNotExists(result.Name + ".tsj")
	fmt.Println("Found! Loading journal...")
	jrnl := ReadJournal(result.Name)
	fmt.Println("Loaded!")
	read := false
	for i := 0; i < len(jrnl.Entries); i++ {
		if jrnl.Entries[i].Subject == result.Subject {
			fmt.Println("")
			fmt.Println("Entry '" + result.Subject + "' <" + jrnl.Entries[i].Hash + "> :")
			fmt.Print(jrnl.Entries[i].Text)
			read = true
		}
	}
	if !read {
		fmt.Println("Error: No entry with subject '" + result.Subject + "'!")
	}
}

// Handles the ability to extract a journal entry as a text file.
func HandleExtractEntry(result CommandLineResult) {
	dieIfNotExists(result.Name + ".tsj")
	fmt.Println("Found! Loading journal...")
	jrnl := ReadJournal(result.Name)
	fmt.Println("Loaded!")
	found := false
	var entry = JournalEntry{}
	for i := 0; i < len(jrnl.Entries); i++ {
		if jrnl.Entries[i].Subject == result.Subject {
			found = true
			entry = jrnl.Entries[i]
		}
	}
	if !found {
		fmt.Println("Error: No such entry '" + result.Subject + "'!")
		os.Exit(-60)
	}
	os.WriteFile(result.Subject + ".txt", []byte(entry.Text), fs.ModeAppend | fs.ModePerm)

}

// Handles the ability to update an existing journal entry by subject.
func HandleUpdateEntry(result CommandLineResult) {
	dieIfNotExists(result.Name + ".tsj")
	fmt.Println("Found! Loading journal...")
	jrnl := ReadJournal(result.Name)
	fmt.Println("Loaded!")
	var entry = -1
	for i := 0; i < len(jrnl.Entries); i++ {
		if jrnl.Entries[i].Subject == result.Subject {
			entry = i
		}
	}
	if entry != -1 {
		dieIfNotExists(result.File + ".txt")
		txt, _ := os.ReadFile(result.File + ".txt")
		jrnl.Entries[entry].Text = string(txt)
		os.Remove(result.File + ".txt")
		fmt.Println("Writing updated journal...")
		os.Remove(result.Name + ".tsj")
		jrnl.Write(result.Name)
		fmt.Println("Done!")
	} else {
		fmt.Println("Error: No entry found with subject '" + result.Subject + "'!")
	}
}

//  Handles the ability to remove an existing journal entry by subject.
func HandleDeleteEntry(result CommandLineResult) {
	dieIfNotExists(result.Name + ".tsj")
	fmt.Println("Found! Loading journal...")
	jrnl := ReadJournal(result.Name)
	fmt.Println("Loaded!")
	var entry = -1
	for i := 0; i < len(jrnl.Entries); i++ {
		if jrnl.Entries[i].Subject == result.Subject {
			entry = i
		}
	}
	if entry == -1 {
		fmt.Println("Error: No such entry with subject '" + result.Subject + "'!")
		os.Exit(-70)
	}
	var newEntries = []JournalEntry{}
	for i := 0; i < len(jrnl.Entries); i++ {
		if jrnl.Entries[i].Subject != result.Subject {
			newEntries = append(newEntries, jrnl.Entries[i])
		}
	}
	jrnl.Entries = newEntries
	fmt.Println("Removed entry '" + result.Subject + "'!")
	fmt.Println("Writing updated journal...")
	os.Remove(result.Name + ".tsj")
	jrnl.Write(result.Name)
	fmt.Println("Done!")
}

// Handles encrypting an entry by subject with the provided password.
func HandleEncryptEntry(result CommandLineResult) {
	dieIfNotExists(result.Name + ".tsj")
	fmt.Println("Found! Loading journal...")
	jrnl := ReadJournal(result.Name)
	fmt.Println("Loaded!")
	var entry = -1
	for i := 0; i < len(jrnl.Entries); i++ {
		if jrnl.Entries[i].Subject == result.Subject {
			entry = i;
			break;
		}
	}
	if entry == -1 {
		fmt.Println("No such entry'" + result.Subject + "'!")
		os.Exit(-72)
	}
	if jrnl.EncryptEntry(&jrnl.Entries[entry], result.Password) {
		fmt.Println("Encrypted! Saving...")
		jrnl.Write(result.Name)
		fmt.Println("Done!")
	} else {
		fmt.Println("Error encrypting entry!")
	}
}

// Handles decrypting an entry by subject with the provided password.
func HandleDecryptEntry(result CommandLineResult) {
	dieIfNotExists(result.Name + ".tsj")
	fmt.Println("Found! Loading journal...")
	jrnl := ReadJournal(result.Name)
	fmt.Println("Loaded!")
	var entry = -1
	for i := 0; i < len(jrnl.Entries); i++ {
		if jrnl.Entries[i].Subject == result.Subject {
			entry = i
			break
		}
	}
	if entry == -1 {
		fmt.Println("No such entry '" + result.Subject + "'!")
		os.Exit(-72)
	}
	if jrnl.Decrypt(&jrnl.Entries[entry], result.Password) {
		fmt.Println("Decrypted! Saving...")
		jrnl.Write(result.Name)
		fmt.Println("Done!")
	} else {
		fmt.Println("Error decrypting entry!")
	}
}