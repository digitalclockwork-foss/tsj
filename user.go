package main

import (
	"encoding/json"
	"fmt"
	"os"
)

type User struct {
	Username string
	Email    string
	Password string
}

func NewUser(username string, email string, password string) *User {
	return &User{
		Username: username,
		Email:    email,
		Password: password,
	}
}

func (u *User) Write(path string) bool {
	_, err := os.Stat(path + ".json")
	if err == nil {
		os.Remove(path + ".json")
	}

	data, err := json.Marshal(u)
	if err != nil {
		fmt.Errorf("Error marshaling user JSON\n")
		os.Exit(-101)
	}
	os.WriteFile(path + ".json", data, 0777)
	return true
}

func ReadUser(path string) *User {
	_, err := os.Stat(path + ".json")
	if err != nil {
		return nil
	}
	data, err := os.ReadFile(path + ".json")
	usr := NewUser("", "", "")
	json.Unmarshal(data, &usr)
	return usr
}