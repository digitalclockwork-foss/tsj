package main

import (
	"fmt"
	"os"

	"github.com/joho/godotenv"
	"github.com/pborman/getopt/v2"
)

func main() {
	godotenv.Load()
	result := DefineGetOpt()

	if result.HelpRoot {
		getopt.Usage()
		os.Exit(0)
	}

	if len(result.Args) < 1 {
		fmt.Println("Must supply a command. Try tsj -h for instructions.")
		os.Exit(-13)
	}

	switch result.Args[0] {
	case ActionNew:
		HandleNew(result)
	case ActionAddEntry:
		HandleAddEntry(result)
	case ActionListActions:
		HandleListEntries(result)
	case ActionViewEntry:
		HandleViewEntry(result)
	case ActionExtractEntry:
		HandleExtractEntry(result)
	case ActionUpdateEntry:
		HandleUpdateEntry(result)
	case ActionDelete:
		HandleDeleteEntry(result)
	case ActionEncrypt:
		HandleEncryptEntry(result)
	case ActionDecrypt:
		HandleDecryptEntry(result)
	default:
		fmt.Println("Error: No such command '" + result.Action + "'!")
	}
}
