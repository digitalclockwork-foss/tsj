package main

import (
	"os"
)

func ExistsFileOrDirectory(path string) bool {
	_, error := os.Stat(path)
	if os.IsNotExist(error) {
		return false
	} else {
		return true
	}
}

func ExistsFile(path string) bool {
	filePtr, error := os.Stat(path)
	if os.IsNotExist(error) {
		return false
	}
	if os.FileInfo.IsDir(filePtr) {
		return false
	}
	return true
}

func ExistsDirectory(path string) bool {
	filePtr, err := os.Stat(path)
	if os.IsNotExist(err) {
		return false
	}
	if os.FileInfo.IsDir(filePtr) {
		return true
	}
	return false
}