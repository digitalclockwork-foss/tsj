package main

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"time"
)

type JournalEntry struct {
	Subject string
	Text string
	Encrypted bool
	CipherText string
	Hash string
}

func NewJournalEntry(subject string, text string) JournalEntry {
	return JournalEntry{
		Subject: subject,
		Text: text,
		Encrypted: false,
		CipherText: "",
		Hash: MakeSHA256(subject + ":" + text),
	}
}

func (j *JournalEntry) SetEntry(subject string, text string) {
	j.Subject = subject
	j.Text = text
	j.Encrypted = false
	j.CipherText = ""
	j.Hash = MakeSHA256(j.Subject + ":" + j.Text)
}

type Journal struct {
	Title string
	Name string
	Key string
	Entries []JournalEntry
}

func NewJournal(title string, name string, seed string) Journal {
	key := MakeSHA256(seed)
	return Journal{
		Title: title,
		Name: name,
		Key: key,
		Entries: []JournalEntry{},
	}
}

func NewJournalNoSeed(title string, name string) Journal {
	return NewJournal(title, name, time.Now().String())
}

func (j *Journal) AddEntry(entry JournalEntry) {
	j.Entries = append(j.Entries, entry)
}

func (j *Journal) Write(name string) bool {
	data, jsonErr := json.Marshal(j)
	if jsonErr != nil {
		fmt.Println("Error marshaling journal '" + j.Title + "'!")
		return false
	}
	err := os.WriteFile(name + ".tsj", data, 0644)
	if err != nil {
		fmt.Println("Error writing journal '" + name + ".tsj'!")
		return false
	}
	return true
}

func ReadJournal(name string) Journal {
	data, err := os.ReadFile(name + ".tsj")
	if err != nil {
		fmt.Println("Error reading journal '" + name + ".tsj'!")
		os.Exit(-41)
	}
	jrnl := NewJournal("", "", "")
	json.Unmarshal(data, &jrnl)
	return jrnl
}

func (j *Journal) EncryptEntry(entry *JournalEntry, password string) bool {
	if entry.Encrypted {
		return false
	}
	text := []byte(entry.Text)
	key := []byte(MakeSHA256Hash(password))
	c, err := aes.NewCipher(key)
	if err != nil {
		fmt.Println("Cipher error.")
		return false
	}
	gcm, err := cipher.NewGCM(c)
	if err != nil {
		fmt.Println("GCM Error.")
		return false
	}
	nonce := make([]byte, gcm.NonceSize())
	if _, err = io.ReadFull(rand.Reader, nonce); err != nil {
		fmt.Println(err)
		return false
	}
	entry.CipherText = string(gcm.Seal(nonce, nonce, text, nil))
	fmt.Println(hex.EncodeToString([]byte(entry.CipherText)[:]))
	entry.CipherText = hex.EncodeToString([]byte(entry.CipherText)[:])
	entry.Encrypted = true
	entry.Text = ""
	return true
}

func (j *Journal) Decrypt(entry *JournalEntry, password string) bool {
	if !entry.Encrypted {
		return false
	}
	key := MakeSHA256Hash(password)
	cpher, err := hex.DecodeString(entry.CipherText)
	if err != nil {
		fmt.Println("Hex decoding error.")
		os.Exit(-81)
	}
	c, err := aes.NewCipher(key)
	if err != nil {
		fmt.Println("Key error.")
		os.Exit(-83)
	}
	gcm, err := cipher.NewGCM(c)
	if err != nil {
		fmt.Println("GCM Error.")
		os.Exit(-84)
	}
	nonceSize := gcm.NonceSize()
	
	if gcm.NonceSize() < nonceSize {
		fmt.Println("Nonce Error.")
		os.Exit(-85)
	}

	nonce, cipherText := cpher[:nonceSize], cpher[nonceSize:]
	text, err := gcm.Open(nil, nonce, cipherText, nil)

	if err != nil {
		fmt.Println("Decryption error.")
		fmt.Println(err)
		os.Exit(-86)
	}

	entry.CipherText = ""
	entry.Encrypted = false
	entry.Text = string(text)

	return true
}