module digitalclockwork.com/tsj

go 1.20

require (
	github.com/joho/godotenv v1.5.1
	github.com/pborman/getopt/v2 v2.1.0
)
