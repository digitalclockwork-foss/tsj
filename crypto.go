package main

import (
	"crypto/sha256"
	"encoding/hex"
)

func MakeSHA256(text string) string {
	hasher := sha256.New()
	hasher.Write([]byte(text))
	hash := hasher.Sum(nil)
	hexDigest := hex.EncodeToString(hash[:])
	return hexDigest
}

func MakeSHA256Hash(text string) []byte {
	hasher := sha256.New()
	hasher.Write([]byte(text))
	return hasher.Sum(nil)
}

func TruncateTo32Bytes(hash string) []byte {
	newText := ""
	x := 0
	hashBytes := []byte(hash)
	for i := 0; i < len(hashBytes); i++ {
		if x < 32 {
			newText = newText + string(hashBytes[i])
			x++
		}
	}
	return []byte(newText)
}