package main

const Version = "Totally Secure Journal(tsj) v0.0.2"

const VersionMajor = 0

const VersionMinor = 0

const VersionRevision = 3

type Action string

const (
	ActionListActions  string = "list"
	ActionNew          string = "new"
	ActionAddEntry     string = "add"
	ActionViewEntry    string = "view"
	ActionExtractEntry string = "extract"
	ActionUpdateEntry  string = "update"
	ActionDelete       string = "delete"
	ActionEncrypt      string = "encrypt"
	ActionDecrypt      string = "decrypt"
)
