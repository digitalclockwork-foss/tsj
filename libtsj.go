package main

import (
	"flag"
	"fmt"
	"os"

	"github.com/pborman/getopt/v2"
)

/**
 * Section Command-Line Interface
 */

 var ActionList [4]string = [4]string{
	"list_actions",
	"Lists the actions you may perform on journal(s).",
	"new",
	"Creates a new Journal named <NAME>",
}

type CommandLineResult struct {
	Action string
	// Section Common
	Title string
	Name string
	User string
	File string
	Subject string
	Args []string
	Encrypted bool
	Password string
	// Section NEW
	NewTitle string
	// Section ADDENTRY
	AEKeep bool
	AEInteractive bool
	// Section Help
	HelpRoot bool
}

func NewCommandLineResult() CommandLineResult {
	return CommandLineResult{
		Action: "",
		Name: "my_journal",
		User: "Joe Schmo and/or Jane Doe",
		NewTitle: "My Journal",
	}
}

func PrintActionList() {
	fmt.Println("Usage: '-action <action>' OR '--action=<action>' Action List:")
	for i := 0; i < len(ActionList); i+=2 {
		fmt.Println("\t\t", ActionList[i], "-", ActionList[i+1])
	}
}

func DieMessageErrorCode(message string, exitCode int) {
	fmt.Println("Error:", message)
	flag.PrintDefaults()
	os.Exit(exitCode)
}

func DefineGetOpt() CommandLineResult {
	result := NewCommandLineResult()
	getopt.Flag(&result.HelpRoot, 'h', "Displays the help screen.")
	var rTitle = getopt.StringLong("title", 't', "My Journal", "The title displayed on the cover of your journal.")
	var rName = getopt.StringLong("name", 'n', "journal", "The name of your journal.")
	var rUser = getopt.StringLong("user", 'u', "anonymous", "The user's name.")
	var rPath = getopt.StringLong("file", 'f', "NULL", "The path to the text file containing the body of your entry.")
	var rSubject = getopt.StringLong("subject", 's', "NULL", "The subject of the journal entry. This will be used to look up your journal entries.")
	var rKeep = getopt.BoolLong("keep", 'k', "false", "An optional flag to keep the original journal entry text file.")
	var rInteractive = getopt.BoolLong("interactive", 'i', "false", "Wether to perform the specified action in interactive mode.")
	var encrypted = getopt.BoolLong("encrypted", 'e', "false", "Wether to encrypt the journal ( note: requires -password to be set. )")
	var password = getopt.StringLong("password", 'p', "", "The password you wish to use to encrypt your journal.-+")
	getopt.Parse()
	result.Args = getopt.Args()
	if len(result.Args) > 0 {
		result.Action = result.Args[0]
	}
	result.Title = *rTitle
	result.Name = *rName
	result.User = *rUser
	result.File = *rPath
	result.Subject = *rSubject
	result.AEKeep = *rKeep
	result.AEInteractive = *rInteractive
	result.Encrypted = *encrypted
	result.Password = *password
	return result
}